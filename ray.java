class ray {
    
    vec3 orig;
    vec3 dir;
    
    public ray(vec3 origin, vec3 direction){
        this.orig = origin;
        this.dir = direction;
    }
    public vec3 origin(){
        return orig;
    }
    public vec3 direction(){
        return dir;
    }
    public vec3 at(double t){
        return dir.multiply(t).add(dir);
    }
    
    
}