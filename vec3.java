class vec3 {
    double x;
    double y;
    double z;

    public vec3() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    public vec3(double e1, double e2, double e3) {
        this.x = e1;
        this.y = e2;
        this.z = e3;
    }
    public vec3 add(vec3 v){
        return new vec3(this.x+v.x, this.y+v.y, this.z+v.z);
    }
    public vec3 subtract(vec3 v){
        return new vec3(this.x-v.x, this.y-v.y, this.z-v.z);
    }
    public vec3 multiply(double d){
        return new vec3(this.x*d, this.y*d, this.z*d);
    }
    
    public vec3 divide(double t){
        return this.multiply(1/t);
    }
    
    public double length(){
        return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
    }
    public vec3 update(double e1, double e2, double e3) {
        this.x = e1;
        this.y = e2;
        this.z = e3;
        return this;
    }
    
    public double x(){return this.x;}
    public double y(){return this.y;}
    public double z(){return this.z;}
    
    public vec3 unit_vector(vec3 v){
        return v.divide(v.length());
    }
    
}

// class unit_vector {
    // public vec3 unit_vector(vec3 v){
        // return v.divide(v.length());
    // }
// }