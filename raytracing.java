class render {
    public static void main(String args[]){
        // Image
        var aspect_ratio = 16.0 / 9.0;
        int image_width = 400;
        int image_height = (int) (image_width/aspect_ratio);
        
        // Camera
        var viewpoint_height = 2.0;
        var viepoint_width = aspect_ratio * viewpoint_height;
        var focal_length = 1.0;
        
        var origin = new vec3(0,0,0);
        var horizontal = new vec3(viepoint_width, 0, 0);
        var vertical = new vec3(0, viewpoint_height, 0);
        var lower_left_corner = origin.subtract(horizontal.divide(2)).subtract(vertical.divide(2)).subtract(new vec3(0, 0, focal_length));
        // System.out.println(origin.x());
        // Render
        
        //vec3 pixel_color = new vec3();
        System.out.print("P3\n" + image_width + " " + image_height + "\n255\n");
        
        for (int j = image_height-1; j >= 0; j--){
            System.err.print("\r"+"Remaining: "+j);
            for (int i = 0; i < image_width; i++){
                var u = (double) i / (image_width-1);
                var v = (double) j / (image_height-1);
                // System.out.println(u + " " + v);
                // ray r = new ray(origin, lower_left_corner.add(horizontal.multiply(u).add(vertical.multiply(v))).subtract(origin));
                ray r = new ray(origin, lower_left_corner.add(horizontal.multiply(u)));
                // System.out.println(r.origin().x() + " " + r.direction().x());
                vec3 pixel_color = ray_color(r);
                // System.out.println("u: " + u + (pixel_color.x)+" "+(pixel_color.y)+" "+(pixel_color.z));
                // System.out.println(r.direction());
                System.out.println((int) (pixel_color.x*255)+" "+(int) (pixel_color.y*255)+" "+(int) (pixel_color.z*255));
            }
        }
        System.err.print("\n");
    }
    private static vec3 ray_color(ray r){
        vec3 unit_direction = r.direction().divide(r.direction().length());
        var t = 0.5*(unit_direction.y() + 1.0);
        return new vec3(1.0, 1.0, 1.0).multiply(1.0-t).add(new vec3(0.5, 0.7, 1.0).multiply(t));
    }
}
